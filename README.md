# README #


## Scripts ##

### Input generation ###
This script is used to create the input data to train or test the Neural Network. In the following an example for the use of the script is shown

```
#!python
python input_generation.py ../../data/raw_april/april_raw_all.tif ../../data/samples/samples_with_background.shp ../../data/subareas_masks/subareas.shp ../../training/ april --ignore_mixed
```
The user has to give the path to the data(orthophoto + ndsm). Additionally, the path to file with the ground truth labels is needed. Since not all data of the national park fits into the graphic card memory it is split into subareas. The shapes of the subareas are given as input to the script. Furthermore, the user can define the output directory. In the example it is named training. Since the two provided orthophotos are taken from different months the user can define a prefix(e.g. april). This prefix is added to each output file to distinguish the files later. If the flag --ignore_mixed is used no annotations for the class "Mischpixel" are created.

As output the script creates for each subarea a hdf5 file containing the network's input for this subarea(normed data + labels[only used during training] + selection_weights[only used during training] + weights[only used during training] ). 

### Classify ###
This script is used to output a per pixel classification of tree species. In the following an example for the use of this script is shown 

```
#!python
python classify.py ../../training/tree_net_classify.prototxt  ../../training/tree_net_snapshot_iter_105000.caffemodel ../../testing/hdf5_list.txt ../../prediction_testing
```
As input the script requires the network structure(tree_net_classify.prototxt) and the learned weights(tree_net_snapshot_iter_105000.caffemodel). Additionally, the path to the text file is needed which lists the paths to the hdf5 files which contain the data is needed. These hdf5 files can be created by the input generation script mentioned above. Finally, the user defines the folder where the output should be stored.

As output the script creates for each input hdf5 file an output hdf5 file containing the prediction for each pixel.

     


### How do I get set up? ###