'''
Created on 14.12.2016

@author: mr42
'''
import sys
from pandas import DataFrame
from scipy.stats import itemfreq
import numpy as np
from plotting.colors import Colors
from plotting.plot import Plot


class DiscretePlot(Plot):
    '''
    classdocs
    '''
        


    def __init__(self, title, data, labels, colors, add_percent=True, ignore_value=0,
                 shared_plot=None):
        '''
        Constructor
        '''
        super(DiscretePlot, self).__init__(title, shared_plot)
        self.colors = Colors()
        self.data = data
        self.legend_info = DataFrame({"label": labels, "color": colors})
        self.add_percent = add_percent
        self.ignore_value = ignore_value
        
        colors = []
        if self.add_percent:
            freq = itemfreq(self.data[self.data!=self.ignore_value])
            all_classes = freq[:,1].sum()
            freq_dict = {}
            for row_id in range(len(freq)):
                freq_dict[freq[row_id, 0]] = freq[row_id, 1]
            
        for class_id, row in self.legend_info.iterrows():
            # use dummy plot to produce labeling
            color = self.colors[row["color"]]
            colors.append(color)
            if self.add_percent and class_id != self.ignore_value:
                if all_classes == 0:
                    percent = 0
                else:    
                    percent = freq_dict.get(class_id, 0) / float(all_classes) * 100
                label = "%s %.2f%%" % (row["label"], percent)
            else:
                label = row["label"]
            if class_id != self.ignore_value:
                self.subplot.plot(sys.maxsize, sys.maxsize, "s", c=color,
                    label=label, markersize=25)
        # color the data
        palette = np.array(colors)
        col_data = palette[self.data]
        # plot the data
        self.subplot.imshow(col_data)
        # plot the legend right to the data plot
        self.subplot.legend(loc='center left', bbox_to_anchor=(1, 0.5), numpoints=1)
        
        