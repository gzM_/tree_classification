'''
Created on 14.12.2016

@author: mr42
'''
import matplotlib.pyplot as plt
from plotting.plot import Plot


class ContinousPlot(Plot):
    '''
    classdocs
    '''
        

    def __init__(self, title, data, cmap=True, shared_plot=None):
        '''
        Constructor
        '''
        super(ContinousPlot, self).__init__(title, shared_plot)
        self.data = data
        cmap = cmap
        
        if cmap:
            im = self.subplot.imshow(self.data, cmap=plt.get_cmap("gnuplot"))
            self.add_colorbar(im)
            #self.fig.colorbar(im, cax=ax_cb)

            
        else:
            self.subplot.imshow(self.data)
        
        
    def add_colorbar(self, im):
        from mpl_toolkits.axes_grid1 import make_axes_locatable
        divider = make_axes_locatable(self.subplot)
        ax_cb = divider.new_horizontal(size="5%", pad=0.1)
        self.fig.add_axes(ax_cb)
        self.fig.colorbar(im, cax=ax_cb)
        ax_cb.yaxis.tick_right()
#         for tl in ax_cb.get_yticklabels():
#             tl.set_visible(False)
        ax_cb.yaxis.tick_right()

