'''
Created on 29.07.2016

@author: mr42
'''
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pylab as pl
import sys
from plotting.colors import Colors
from scipy.stats import itemfreq


class DataViewer(object):
    '''
    classdocs
    '''

    def __init__(self, format="png", dpi=900):
        '''Create a DataViewer.
        
        :param format: The format to save visualizations as file.
        :param dpi: The resolution in dpi when saving visualizations as file.
        '''
        self.fig = plt.figure()
        self.fig.patch.set_facecolor('white')
        self.colors = Colors()
        self.format = format
        self.dpi = dpi
        
    def show(self):
        plt.show()

    def close(self):
        plt.close()

    def add_continous(self, data, plot_coordinates, share_axis=None, cmap=True):
        '''Visualize continous data using the imshow method.
        
        :param data: The data to visualize.
        :type data: np.ndarray
        :param save_to: If given, the path without extension to save the
                        visualization as file.
        :type save_to: str
        :param suppress_show: If true, the visualization is shown immediately.
                              Otherwise it is only saved as file.
        :type suppress_show: boolean
        :param title: The title for the visualization.
        :type title: str
        :param cmap: If true a colormap is added to the visualization.
        :type cmap: boolean

        '''
        if share_axis:
            subplot = self.fig.add_subplot(plot_coordinates[0],
                                           plot_coordinates[1],
                                           plot_coordinates[2],
                                           sharex=share_axis,
                                           sharey=share_axis)
        else:
            subplot = self.fig.add_subplot(plot_coordinates[0],
                                           plot_coordinates[1],
                                           plot_coordinates[2])
        if cmap:
            plt.imshow(data, cmap=plt.get_cmap("gnuplot"))
            plt.colorbar(orientation='vertical')
        else:
            plt.imshow(data)
        return subplot

    def to_file(self, path, legend=None):
        '''Save the figure as file to the given path.
        :param fig: The figure to save.
        :type fig: plt.figure
        :param path: The path for the file without the extension. 
        :type path: str
        :param legend: If the figure has an extra defined legend, this legend 
                       should be given here.
        :type legend: plt.legend 
        '''
        path += "." + self.format
        # save as pdf
        if self.format=="pdf":
            pp = PdfPages(path)
            if legend is not None:
                pp.savefig(self.fig, bbox_extra_artists=(legend,),
                           bbox_inches='tight',
                           dpi=self.dpi)
            else:
                pp.savefig(self.fig, bbox_inches='tight', dpi=self.dpi)
            pp.close()
        # save as png
        elif self.format =="png":
            if legend is not None:
                self.fig.savefig(path, bbox_extra_artists=(legend,),
                            bbox_inches='tight',
                            format=self.format, dpi=self.dpi)
            else:
                self.fig.savefig(path, bbox_inches='tight', format=self.format,
                            dpi=self.dpi)
                    
    def add_class_labels(self,data, vis_info, plot_coordinates, share_axis=None,
                         add_percent=True, ignore_value=0):
        '''Visualize class labels using the imshow method.
        
        It is assumed that the class labels are integers beginning at 0. Foreach
        new class the last known class label is increased by 1. E.g. for 
        four classes the labels are 0,1,2,3 but not 0,5,6,12. Otherwise
        the coloring of the data and the legend creation does not work correctly.
        
        :param data: The data to visualize.
        :type data: np.ndarray
        :param save_to: If given, the path without extension to save the
                        visualization as file.
        :type save_to: str
        :param vis_info: A pandas dataframe having a color and label column.
                         It defines the color and label foreach class.
                         This information is used to color the data and 
                         create a legend.
        :type vis_info: Dataframe
        :param suppress_show: If true, the visualization is shown immediately.
                              Otherwise it is only saved as file.
        :type suppress_show: boolean
        :param title: The title for the visualization.
        :type title: str
        :param add_percent: If true a the percentage of each class is computed
                            and added to the visualization.
        :type add_percent: boolean
        :param ignore_value: All labels with the given value are ignored in the
                             percentage computation.
        :type ignore_value: int

        '''
        if share_axis:
            subplot = self.fig.add_subplot(plot_coordinates[0],
                                           plot_coordinates[1],
                                           plot_coordinates[2],
                                           sharex=share_axis,
                                           sharey=share_axis)
        else:
            subplot = self.fig.add_subplot(plot_coordinates[0],
                                           plot_coordinates[1],
                                           plot_coordinates[2])
        # Add legend
        colors = []
        if add_percent:
            freq = itemfreq(data[data!=ignore_value])
            all_classes = freq[:,1].sum()
            freq_dict = {}
            for row_id in range(len(freq)):
                freq_dict[freq[row_id, 0]] = freq[row_id, 1]
            
        for class_id, row in vis_info.iterrows():
            # use dummy plot to produce labeling
            color = self.colors[row["color"]]
            colors.append(color)
            if add_percent and class_id != ignore_value:
                if all_classes == 0:
                    percent = 0
                else:    
                    percent = freq_dict.get(class_id, 0) / float(all_classes) * 100
                label = "%s %.2f%%" % (row["label"], percent)
            else:
                label = row["label"]
            if class_id != ignore_value:
                pl.plot(sys.maxsize, sys.maxsize, "s", c=color,
                    label=label, markersize=12)
        # color the data
        palette = np.array(colors)
        col_data = palette[data]
        # plot the data
        plt.imshow(col_data)
        # plot the legend right to the data plot
        legend = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5),
                            numpoints=1)
        return subplot

        
        