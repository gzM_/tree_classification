'''
Created on 20.12.2016

@author: mr42
'''
import matplotlib.pyplot as plt


class Plot(object):
    '''
    classdocs
    '''


    def __init__(self, title, shared_plot=None, box_forced=True):
        '''
        Constructor
        '''
        self.fig = plt.figure()
        self.fig.canvas.set_window_title(title)
        if shared_plot:
            self.subplot = self.fig.add_subplot(121, sharex=shared_plot.subplot,
                                           sharey=shared_plot.subplot)
        else:
            self.subplot = self.fig.add_subplot(121)
        
        if box_forced:
            self.subplot.set_adjustable('box-forced')