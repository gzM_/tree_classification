'''
Created on 05.10.2016

@author: mr42
'''

class Colors(dict):
    '''
    classdocs
    '''


    def __init__(self):
        '''Create a Colors object defining the most used colors in matplotlib.
        '''
        super(Colors, self).__init__() 
        self.update({'g': [0.0, 0.5, 0.0], '0.70': (0.7, 0.7, 0.7),
                     '.15': (0.15, 0.15, 0.15),
          'b': [0.0, 0.0, 1.0], 'k': [0.0, 0.0, 0.0],
          'w': [1.0, 1.0, 1.0],'m': [0.75, 0, 0.75],
          'c': [0.0, 0.75, 0.75], 'r': [1.0, 0.0, 0.0], 'y': [0.75, 0.75, 0]})
        