'''
Created on 09.12.2016

@author: mr42
'''
from argparse import ArgumentParser
import h5py
import matplotlib.pyplot as plt
import matplotlib
from plotting.discrete_plot import DiscretePlot
from plotting.continous_plot import ContinousPlot
import numpy as np
import pandas as pd
from translations.tree_species_translator import TreeSpeciesTranslator


def get_data(h5_paths, key, dtype = np.float32):
    for h5_path in h5_paths:
        with h5py.File(h5_path, "r") as f:
            data = f.get(key, )
            if data is not None:
                return data[()].astype(dtype)
    raise KeyError(key)    

def main(args):
    matplotlib.rcParams.update({'font.size': 26})
    h5_paths = [args.input, args.prediction, args.analyse]
    class_names = pd.read_csv(args.class_names, index_col=0)
    # Visualize data
    data = get_data(h5_paths, "data")
    data = data.swapaxes(1,2).swapaxes(2,3)[0]
    cp = ContinousPlot("rgb", data[:,:,0:3], cmap=False)
    cp1 = ContinousPlot("infrared", data[:,:,3], shared_plot=cp)
    cp2 = ContinousPlot("height", data[:,:,4], shared_plot=cp)
    
    
    selection_weights = get_data(h5_paths, "selection_weights")[0,0]
    cp3 = ContinousPlot("selection_weights", selection_weights, shared_plot=cp)
    
    weights = get_data(h5_paths, "weights")[0,0]
    cp4 = ContinousPlot("weights", selection_weights, shared_plot=cp)
    
    # Visualize labels
    data = get_data(h5_paths, "labels", dtype=np.int8)[0,0]
    # TODO: replace BA by BA_EN for report
    tree_species_translator = TreeSpeciesTranslator()
    labels = tree_species_translator.speciesToEn(list(class_names["BA"]))
    ignore_value = len(class_names)
    data[selection_weights==0] = ignore_value
    labels.append("unknown")
    colors = ["c", "g", "r","b","m", "y", "k", "w"]
    dp = DiscretePlot("labels", data, labels=labels, colors=colors,
                 ignore_value=ignore_value, shared_plot=cp, add_percent=False)
    
    data = get_data(h5_paths, "prediction", dtype=np.int8)
    labels = tree_species_translator.speciesToEn(list(class_names["BA"]))
    ignore_value = len(class_names)
    data[selection_weights==0] = ignore_value
    labels.append("unknown")
    colors = ["c", "g", "r","b","m", "y", "k", "w"]
    dp = DiscretePlot("prediction", data, labels=labels, colors=colors,
                 ignore_value=ignore_value, shared_plot=cp, add_percent=False)
    
    data = get_data(h5_paths, "prediction", dtype=np.int8)
    labels = tree_species_translator.speciesToEn(list(class_names["BA"]))
    ignore_value = len(class_names)
    labels.append("unknown")
    colors = ["c", "g", "r","b","m", "y", "k", "w"]
    dp = DiscretePlot("full prediction", data, labels=labels, colors=colors,
                 ignore_value=ignore_value, shared_plot=cp, add_percent=False)
    
    plt.show()

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(dest="input", type=str,
                        help=("The path to the hdf5 file "
                              "containing the input data for the network."))
    parser.add_argument(dest="prediction", type=str,
                        help=("The path to the hdf5 file "
                              "containing the prediction of the network."))
    parser.add_argument(dest="analyse", type=str,
                        help=("The path to the hdf5 file "
                              "containing the analyse results."))
    parser.add_argument(dest="class_names", type=str,
                        help=(("The path to the csv file containing the "
                        "mapping between class names and ids.")))
    args = parser.parse_args()
    main(args)
    
    
    