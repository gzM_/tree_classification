'''
Created on 15.06.2016

@author: mr42

running on cluster
qsub -I -l nodes=1:nvidiaTITAN:ppn=1,gpus=1,mem=4gb,walltime=1:00:00:00 -q default-gpu

'''

from argparse import ArgumentParser
from file.path import create_dir
import h5py
import caffe
import numpy as np
import os
from machinelearning.classifier import Classfier
from log import get_logger
import pandas as pd


def main(args):
    create_dir(args.output)
    log = get_logger()
    c = Classfier(args.model, args.weights, gpu_mode=args.gpu)

    log.info("Read text file listing the filenames of the input hdf5 files.")
    input_files = pd.read_csv(args.input, names=["file_name"])
    dir_input = os.path.split(os.path.abspath(args.input))[0]
    h5_list_file = open(os.path.join(args.output, 'hdf5_list.txt'), 'w')
    for gid, input_file in input_files.iterrows():
        with h5py.File(os.path.join(dir_input,
                                    input_file["file_name"]),'r') as f:
            log.info("Start prediction of hdf5 file %s." %
                     input_file["file_name"])
            prediction = c.predict(f["data"][:], (5, 540, 540), (7, 356, 356))
            file_name_result = "results_%s.h5" % os.path.splitext(
                                              input_file["file_name"])[0]
            log.info("Write results to %s." % file_name_result)
            h5_list_file.write(file_name_result + "\n")
            with h5py.File(os.path.join(args.output, file_name_result),
                           'w') as fo:
                fo["prediction"] = prediction
    h5_list_file.close()
            

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(dest="model", type=str,
                        help=("The path to the network model."))
    parser.add_argument(dest="weights", type=str,
                        help=("The path to the weight file."))
    parser.add_argument("-g", "--gpu", dest="gpu", type=bool,
                        default=True,
                        help=("Use gpu for caffe or not"))
    parser.add_argument(dest="input", type=str,
                        help=("The path to the text file listing the file "
                              "names of the hdf5 files "
                              "containing the input data for the network."))
    parser.add_argument(dest="output", type=str,
                        help=("The output folder."))
    args = parser.parse_args()
    main(args)
    