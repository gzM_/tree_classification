'''
Created on 29.07.2016

@author: mr42

Script to visualize all input and output data of unet.

'''
from argparse import ArgumentParser
from file.path import create_dir
import os
import h5py
from log import get_logger
import pandas as pd
import numpy as np
import numpy.ma as ma
from parallel import ProcessManager
from sklearn.metrics.classification import confusion_matrix



def save_to_hdf5(path, name, data, mode= "a"):
    with h5py.File(path, mode) as f:
        f[name] = data

def measurement_to_file(measurement, save_to, class_names):
    header = ",".join(list(class_names["BA"]))
    np.savetxt(save_to,
               np.expand_dims(measurement, axis=0),
               delimiter=',', header=header, fmt="%.6f")

def compute_precision(true_positive, false_positive):
    result = true_positive / (true_positive + false_positive) 
    return np.nan_to_num(result)


def compute_accuracy(true_positive, true_negative, total_population):
    print("tp: " , true_positive)
    print("tn: " , true_negative)
    print("top: " , total_population)
    result = (true_positive + true_negative) / total_population
    print("res: ", result)
    return np.nan_to_num(result)

def compute_recall(true_positive, false_negative):
    result = true_positive / (true_positive + false_negative)
    return np.nan_to_num(result)

def analyse_batch(args, input_file, prediction_file, class_names):
    log = get_logger()
    batch_name = os.path.splitext(os.path.basename(input_file))[0]
    log.info("Process batch %s" % batch_name)

    with h5py.File(input_file,'r') as f:
        analyse_path = "analyse_%s.h5" % batch_name
        h5_res_path = os.path.join(args.output, analyse_path)

        name = "selection_weights"
        selection_weights = f[name][()][0,0]

        name = "labels"
        ground_truth = f[name][()][0,0].astype(np.int8)
        
    # prediction    
    with h5py.File(prediction_file,'r') as f:  
        name = "prediction"
        prediction = f[name][()].astype(np.int8)
            
        log.info("Compute difference between prediction and ground truth")
        correct_mask = ma.masked_where(prediction == ground_truth,
                                       prediction)
        false_mask = ma.masked_where(prediction != ground_truth,
                                     prediction)
#     
        unknown_id = 0
        correct_id = 1
        false_id = 2
        # difference all classes
        dif = np.zeros(prediction.shape, dtype=np.int8)
        dif[correct_mask.mask] = correct_id
        dif[false_mask.mask] = false_id
        dif[selection_weights == 0] = unknown_id
        
        # difference per class
        dif_per_class = {}
        for class_id, row in class_names.iterrows():
            class_name = row["BA"]
            #if class_name != "Unknown":
            not_class_mask = ma.masked_where(ground_truth != class_id,
                                             ground_truth)
            dif_per_class[class_name] = np.copy(dif)
            dif_per_class[class_name][not_class_mask.mask] = unknown_id
            
        name = "difference"
        save_to_hdf5(h5_res_path, name, dif, mode="w")
        del dif
        
        # per class
        for class_name, dif_class in dif_per_class.items():
            name_ext = "difference_%s" % class_name
            save_to_hdf5(h5_res_path, name_ext, dif_class)
        
        # true positive/negative and total population per class
        total_population = np.count_nonzero(selection_weights == 1)
        true_positive = np.zeros(len(class_names))
        true_negative = np.zeros(len(class_names))
        false_positive = np.zeros(len(class_names))
        false_negative = np.zeros(len(class_names))
        
        log.info(("Compute and visualize true positive/negative and total "
                "population of batch"))
        
        for class_id, row in class_names.iterrows():
            class_name = row["BA"]
            # true_pos_A = intersection of ground_truth_A and prediction_A
            true_positive_plot = np.zeros(prediction.shape, dtype=np.int8)
            dif = dif_per_class.pop(class_name)
            true_positive_plot[dif == correct_id] = correct_id
            
            true_positive[class_id] = np.count_nonzero(true_positive_plot == 
                                                       correct_id)
            name_ext = "true_positive_%s" % class_name
            save_to_hdf5(h5_res_path, name_ext, true_positive_plot)
            del true_positive_plot
            # false_neg_A = intersection of ground_truth_A and prediction_not_A
            false_negative_plot = np.zeros(prediction.shape, dtype=np.int8)
            false_negative_plot[dif == false_id] = correct_id
            false_negative_plot[ground_truth != class_id] = unknown_id
            del dif
            
            false_negative[class_id] = np.count_nonzero(false_negative_plot == 
                                                       correct_id)
            name_ext = "false_negative_%s" % class_name
            save_to_hdf5(h5_res_path, name_ext, false_negative_plot)
            del false_negative_plot
            
            # false_pos_A = intersection of ground_truth_not_A and prediction_A
            false_positive_plot = np.zeros(prediction.shape, dtype=np.int8)
            false_positive_plot[prediction == class_id] = correct_id
            false_positive_plot[ground_truth == class_id] = unknown_id
            false_positive_plot[selection_weights == 0] = unknown_id
            false_positive[class_id] = np.count_nonzero(false_positive_plot ==
                                                       correct_id)
            name_ext = "false_positive_%s" % class_name
            save_to_hdf5(h5_res_path, name_ext, false_positive_plot)
            del false_positive_plot
            # true_neg_A = intersection of ground_truth_not_A and prediction_not_A
            true_negative_plot = np.zeros(prediction.shape, dtype=np.int8)
            true_negative_plot[ground_truth != class_id] = correct_id
            true_negative_plot[prediction == class_id] = unknown_id
            true_negative_plot[selection_weights == 0] = unknown_id
            true_negative[class_id] = np.count_nonzero(true_negative_plot ==
                                                       correct_id)
            
            name_ext = "true_negative_%s" % class_name
            save_to_hdf5(h5_res_path, name_ext, true_negative_plot)
            del true_negative_plot
            
    log.info("True positive of batch is %s." % (str(true_positive),))
    log.info("True negative of batch is %s." % (str(true_negative),))
    log.info("False positive of batch is %s." % (str(false_positive),))
    log.info("False negative of batch is %s." % (str(false_negative),))
    log.info("Total population of batch is %d." % (total_population,))
    
    accuracy = compute_accuracy(true_positive, true_negative, total_population)
    log.info("Accuracy of batch is %s." % (str(accuracy),))
    measurement_to_file(measurement=accuracy,
                        save_to=os.path.join(args.output,
                                              "accuracy_%s.csv" % batch_name),
                        class_names=class_names)
    
    precision = compute_precision(true_positive, false_positive)
    log.info("Precision of batch is %s." % (str(precision),))
    measurement_to_file(measurement=precision,
                        save_to=os.path.join(args.output,
                                              "precision_%s.csv" % batch_name),
                        class_names=class_names)
    
    recall = compute_recall(true_positive, false_negative)
    log.info("Recall of batch is %s." % (str(recall),))
    measurement_to_file(measurement=recall,
                        save_to=os.path.join(args.output,
                                              "recall_%s.csv" % batch_name),
                        class_names=class_names)
    
    log.info(("Compute confusion matrix of batch"))
    confusion_matrix = np.zeros((len(class_names["BA"]),
                                 len(class_names["BA"])))
    for class_id, row in class_names.iterrows():
        for class_id2, row2 in class_names.iterrows():
            class_predicted_as_class2_plot = np.zeros(prediction.shape,
                                                      dtype=np.int8)
            class_predicted_as_class2_plot[ground_truth == class_id] = correct_id
            class_predicted_as_class2_plot[prediction != class_id2] = unknown_id
            class_predicted_as_class2_plot[selection_weights == 0] = unknown_id
            confusion_matrix[class_id, class_id2] = np.count_nonzero(class_predicted_as_class2_plot == 
                                                                     correct_id)
    save_to_hdf5(h5_res_path, "confusion_matrix", confusion_matrix)
    
    
    species_population = np.zeros(len(class_names))
    for class_id, row in class_names.iterrows():
        species_population_plot = np.zeros(prediction.shape,
                                                      dtype=np.int8)
        species_population_plot[ground_truth == class_id] = correct_id
        species_population_plot[selection_weights == 0] = unknown_id
        species_population[class_id] = np.count_nonzero(species_population_plot
                                                         == correct_id)
        
    save_to_hdf5(h5_res_path, "species_population", species_population)
        
    return (true_positive, true_negative, false_positive, false_negative,
            total_population, confusion_matrix, species_population)
            


def main(args):
    log = get_logger()
    create_dir(args.output)
    # The hdf5 file to save the visualization matrices
    #vis_h5 = h5py.File(os.path.join(args.output, "visualizations.h5"),'w')
    
    log.info("Read class name information from the given csv file.")
    class_names = pd.read_csv(args.class_names, index_col=0)
    
#    log.info("Read tif file.")
#     r = RasterReader()
#     mapper = Mapper(r.get_geo_trans(args.raster))
#     data = r.get_data(args.raster, norm=False) 
    
    log.info("Read text file listing the filenames of the input hdf5 files.")
    input_files = pd.read_csv(args.input, names=["file_name"])
    dir_input = os.path.split(os.path.abspath(args.input))[0]
    prediction_files = pd.read_csv(args.prediction, names=["file_name"])
    dir_prediction = os.path.split(os.path.abspath(args.prediction))[0]
    
    log.info("Analyse the data of the input hdf5 files.")
    
    pm = ProcessManager()
    
    for batch_id, input_file in input_files.iterrows():
        path_prediction_file =os.path.join(dir_prediction, 
                                           prediction_files["file_name"].iloc[
                                                                    batch_id])
        path_input_file = os.path.join(dir_input, input_file["file_name"])
        pm.add_task(analyse_batch, args=(args, path_input_file,
                                           path_prediction_file, class_names))

    res = pm.execute(nproc=args.cores)
    true_positive = np.zeros(len(class_names))
    true_negative = np.zeros(len(class_names))
    false_positive = np.zeros(len(class_names))
    false_negative = np.zeros(len(class_names))
    confusion_matrix = np.zeros((len(class_names["BA"]),
                                 len(class_names["BA"])))
    species_population = np.zeros(len(class_names))
    
    total_population = 0
    for (true_pos_batch, true_neg_batch, false_pos_batch, false_neg_batch, 
         total_pop_batch, confusion_matrix_batch, species_population_batch) in res:
        true_positive += true_pos_batch
        true_negative += true_neg_batch
        false_positive += false_pos_batch
        false_negative += false_neg_batch
        total_population += total_pop_batch
        confusion_matrix += confusion_matrix_batch
        species_population += species_population_batch
        
    log.info("True positive is %s." % (str(true_positive),))
    log.info("True negative is %s." % (str(true_negative),))
    log.info("False positive is %s." % (str(false_positive),))
    log.info("False negative is %s." % (str(false_negative),))
    log.info("Total population is %d." % (total_population,))

    save_to_hdf5(os.path.join(args.output, "analyse.h5"),
                              "confusion_matrix", confusion_matrix, mode="w")
    save_to_hdf5(os.path.join(args.output, "analyse.h5"),
                              "species_population", species_population)

    accuracy = compute_accuracy(true_positive, true_negative, total_population)
    measurement_to_file(measurement=accuracy,
                        save_to=os.path.join(args.output, "accuracy.csv"),
                        class_names=class_names)
    
    precision = compute_precision(true_positive, false_positive)
    measurement_to_file(measurement=precision,
                        save_to=os.path.join(args.output, "precision.csv"),
                        class_names=class_names)
    
    recall = compute_recall(true_positive, false_negative)
    measurement_to_file(measurement=recall,
                        save_to=os.path.join(args.output, "recall.csv"),
                        class_names=class_names)
 
if __name__ == '__main__':
    parser = ArgumentParser()
#     parser.add_argument(dest="raster", type=str,
#                         help=("The path to the tif file containing the "
#                         "original data"))
    parser.add_argument(dest="input", type=str,
                        help=("The path to the text file listing the file "
                              "names of the hdf5 files "
                              "containing the input data for the network."))
    parser.add_argument(dest="prediction", type=str,
                        help=("The path to the text file listing the file "
                              "names of the hdf5 files "
                              "containing the prediction of the network."))
    parser.add_argument(dest="class_names", type=str,
                        help=(("The path to the csv file containing the "
                        "mapping between class names and ids.")))
    parser.add_argument(dest="output", type=str,
                        help=(("The output folder where to store the "
                        "analyse results.")))
    parser.add_argument("-c", "--cores", dest="cores", type=int,
                        default=4,
                        help=("Defines how many cores are used for the "
                              "visualizations."))
    args = parser.parse_args()
    main(args)