'''
Created on 25.10.2016

@author: mr42
'''
from argparse import ArgumentParser
from gis.shape_reader import ShapeReader
import numpy as np
from file.path import create_dir
import os


def main(args):
    create_dir(args.output)
    sr = ShapeReader()
    #val = sr.get_data(args.validation, geometry="point")
    grid = sr.get_data(args.grid, parse_geometry=False)
    grid = grid.drop(np.nonzero(grid["PKTCNT"] < 1)[0])
    grid.to_csv(os.path.join(args.output, "validation.csv"))

            #(p[0],p[1])
    

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(dest="grid", type=str,
                        help=("The path to the shape file, "
                              "describing the grid."))
    parser.add_argument(dest="output", type=str,
                    help=("The output folder."))
    args = parser.parse_args()
    main(args)