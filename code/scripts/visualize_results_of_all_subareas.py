'''
Created on 22.02.2017

@author: mr42
'''
from argparse import ArgumentParser
import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import matplotlib.patheffects as PathEffects
from translations.tree_species_translator import TreeSpeciesTranslator


def main(args):
    matplotlib.rcParams.update({'font.size': 40})
    with h5py.File(args.analyse, "r") as f:
        data = f.get("confusion_matrix", )[()].astype(np.float32)
        #data = data[1:, 1:]
        # compute normed confusion matrix
        norm_conf = []
        for i in data:
            # sum of row
            a = sum(i, 0)
            normed_row = []
            for j in i:
                normed_row.append((float(j)/float(a)) * 100)
            norm_conf.append(normed_row)
        fig = plt.figure()
        plt.clf()
        ax = fig.add_subplot(111)
        ax.set_aspect(1)
        # plot normed confusion matrix
        res = ax.imshow(np.array(norm_conf), cmap=plt.cm.jet, 
                        interpolation='nearest')
        
        width, height = data.shape
        
        # add the percentages as annotations
        for x in xrange(width):
            for y in xrange(height):
                an = ax.annotate('%.1f' % norm_conf[x][y], xy=(y, x), 
                            horizontalalignment='center',
                            verticalalignment='center', color='white',
                            fontweight="bold")
                an.set_path_effects([PathEffects.withStroke(linewidth=6,
                                                        foreground="black")])
        
        cb = fig.colorbar(res)
        
        class_names = pd.read_csv(args.class_names, index_col=0)

        tree_species_translator = TreeSpeciesTranslator()
        plt.xticks(range(width),
                   tree_species_translator.speciesToEn(list(class_names["BA"])))
        
        plt.yticks(range(height),
                   tree_species_translator.speciesToEn(list(class_names["BA"])))
        plt.show()



if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(dest="analyse", type=str,
                        help=("The path to the hdf5 file "
                              "containing the results of all sub areas."))
    parser.add_argument(dest="class_names", type=str,
                        help=(("The path to the csv file containing the "
                        "mapping between class names and ids.")))
    args = parser.parse_args()
    main(args)