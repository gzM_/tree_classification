#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 25.10.2016

@author: mr42
'''
from argparse import ArgumentParser
from gis.shape_reader import ShapeReader
import numpy as np
from file.path import create_dir
import os


def main(args):
    create_dir(args.output)
    sr = ShapeReader()
    #val = sr.get_data(args.validation, geometry="point")
    validation = sr.get_data(args.validation, parse_geometry=False)
    validation.replace(to_replace={"BA": {"Lä": "Lae", "Bu": "LB",
                                          "Kie": "Ki", "JLä": "Lae"}},
                       inplace=True)
    validation.to_csv(os.path.join(args.output, "validation_corrected.csv"))
    
            #(p[0],p[1])
    

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(dest="validation", type=str,
                        help=("The path to the shape file, "
                              "describing the validation samples."))
    parser.add_argument(dest="output", type=str,
                    help=("The output folder."))
    args = parser.parse_args()
    main(args)