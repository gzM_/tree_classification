'''
Created on Nov 4, 2015

@author: mr42
'''

from argparse import ArgumentParser
import re
import numpy as np
import matplotlib.pyplot as plt
import matplotlib


def add_subplot(rows, cols, identifier, data, legend, x_label, y_label, color,
                data_x = None):
    plt.subplot(rows, cols, identifier)
    if data_x is not None:
        plt.plot(data_x, data, color, label=legend)
    else:
        plt.plot(data, color, label=legend)
    plt.ylabel(x_label)
    plt.xlabel(y_label)
    plt.legend(loc="best")


def main(args):
    matplotlib.rcParams.update({'font.size': 26})
    #create_dir(args.output)
    fdata = open(args.logfile, "r")
    train_loss = []
    test_loss = []

    for line in fdata:
        m = re.search('.* Iteration .* loss = (.+?)\s', line)
        if m:
            found = m.group(1)
            train_loss.append(float(found))

        m = re.search('.* Test net output .* loss.* = (.+?)\s', line)
        if m:
            found = m.group(1)
            test_loss.append(float(found))
            
    train_loss = np.array(train_loss)
    fig = plt.figure()
    add_subplot(1, 3, 1, train_loss, "train loss", 'Loss value', "#Iterations",
                'b')
    train_loss_mean = np.convolve(train_loss, np.ones((41,))/41, mode='valid')
    add_subplot(1, 3, 1, train_loss_mean, "train loss mean", 'Loss value',
                "#Iterations", 'g')
    
    plt.ylim([0, train_loss.max()])
    plt.show()
 

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(dest="logfile", type=str,
                        help='Path to the caffe log file')
    args = parser.parse_args()
    main(args)