'''
Created on 18.05.2016

@author: mr42
'''

from gis.shape_reader import ShapeReader
from gis.raster_reader import RasterReader
from gis.mapper import Mapper
from argparse import ArgumentParser
from file.path import create_dir
import os
import h5py
from machinelearning.samples import Samples
from machinelearning.annotation import Annotation
import numpy as np
from log import get_logger
from gis.mask import Mask
from pandas import Series
import gdal


def main(args):
    log = get_logger()
    create_dir(args.output)
    r = RasterReader()
    sr = ShapeReader()
    geo_trans = r.get_geo_trans(args.data)
    mapper = Mapper(geo_trans)
    log.info("Read shape file containing information about subareas.")
    subarea_mask = Mask(sr.get_data(args.subareas), mapper)
    log.info("Read raster data subarea wise, normalize it and write it to hdf5.")
    
    h5_list_file = open(os.path.join(args.output,
                         'hdf5_list_%s.txt' % args.month), 'w')
    for s_id, (region_x, region_y) in enumerate(subarea_mask):
        log.info("Read raster data of subarea %d" % s_id)
        data_normed = r.get_data(args.data, xoff=region_x[0],
                                 yoff=region_y[0],
                                 xsize=region_x[1] - region_x[0],
                                 ysize=region_y[1] - region_y[0])
        file_name = "%s_both%d.h5" % (args.month, s_id)
        h5_list_file.write(file_name + "\n")
        log.info("Write data to the file %s" % file_name)
        with h5py.File(os.path.join(args.output, file_name), "w") as f:
            f["data"] = np.expand_dims(data_normed, axis=0)
    h5_list_file.close()
   
    log.info("Read shape file containing information about samples.")
    samples = Samples(sr.get_data(args.samples), ignore_mixed=args.ignore_mixed)
    samples.to_csv(os.path.join(args.output, "samples.csv"))
    log.info("Safe mapping between class name and class id to csv.")
    class_names = samples.data[["BA_id", "BA", "frequ"]].drop_duplicates()
    class_names.sort_values(by="BA_id").to_csv(os.path.join(
                                args.output, "class_names.csv"), index=False)
    #samples_sets = samples.split()
    log.info("Create unet annotations.")
    raster = gdal.Open(args.data)
    ann = Annotation(samples, mapper, raster.RasterXSize,
                          raster.RasterYSize)
    
    ann_split = {}
    for name in ann.keys():
        an_data = ann.pop(name)
        ann_split[name] = subarea_mask.get_masked_area(an_data)

    
    log.info("Save annotations to hdf5 file.")
    for s_id in range(len(subarea_mask)):
        file_name = "%s%d.h5" % (args.month, s_id)
        with h5py.File(os.path.join(args.output, file_name), "a") as f:
#             f["data"] = np.expand_dims(np.concatenate((data_split[batch],
#                                        height_split[batch]), axis=0), 
#                                        axis=0)
            for name, an_data in ann_split.items():
                f[name] = np.expand_dims(an_data[s_id], axis=0)
    
            
        
if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(dest="data", type=str,
                        help=("The path to the raster file containing the data."))
    parser.add_argument(dest="samples", type=str,
                        help=("The path to the samples file, "
                              "describing the training samples."))
    parser.add_argument(dest="subareas", type=str, default="",
                        help=("The path to the shape file, describing the "
                        "subareas."))
    parser.add_argument(dest="output", type=str,
                        help=("The output folder."))
    parser.add_argument(dest="month", type=str,
                        help=("Name of month."))
    parser.add_argument("--ignore_mixed", dest="ignore_mixed", default=False,
                        action='store_true',
                        help=("If true the class mixed class is ignored, "
                              "otherwise not."))
    args = parser.parse_args()
    main(args)