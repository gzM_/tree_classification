'''
Created on 22.04.2016

@author: mr42
'''

import logging
from logging import *
import sys
import multiprocessing
from colorlog import ColoredFormatter


def get_logger(logfile=None, level=None):
    logger = multiprocessing.get_logger()
    # Add default console handlers for new loggers.
    if len(logger.handlers) == 0:
        # first remove handlers
        for h in logger.handlers:
            logger.removeHandler(h)
        ch = logging.StreamHandler(sys.stdout)
        fmt = ColoredFormatter('[%(log_color)s%(levelname)s/%(processName)s] %(message)s')
        ch.setFormatter(fmt)
        logger.addHandler(ch)
        
        logger.setLevel("INFO")

    if logfile is not None:
        fh = logging.FileHandler(logfile)
        fmt = logging.Formatter('[%(asctime)-15s/%(levelname)s/%(processName)s] %(message)s')
        fh.setFormatter(fmt)
        logger.addHandler(fh)

    if level is not None:
        logger.setLevel(level.upper())

    return logger
