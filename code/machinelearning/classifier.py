'''
Created on 25.07.2016

@author: mr42
'''
import numpy as np
import caffe
from log import get_logger
from scipy.ndimage.interpolation import rotate


class Classfier(caffe.Net):
    '''
    classdocs
    '''

    def __init__(self, model, weights, show_output=["prediction"],
                 gpu_mode=True):
        caffe.Net.__init__(self, model, weights, caffe.TEST)
        self.show_output = show_output
  
        if gpu_mode:
            caffe.set_mode_gpu()
        else:
            caffe.set_mode_cpu() 

    def predict(self, data, input_shape, output_shape):
        """Classify the data    
        
        :param data: The data to classify. The shape should follow the format
                     (samples, channels, height, width).
        :type data: np.array
        :param input_shape: The input shape of the network. The shape should
                            follow the format (channels, height, width).
        :type input_shape: tuple
        :param output_shape: The output shape of the network. The shape should
                            follow the format (channels, height, width).
        :type output_shape: tuple
        :returns: np.array The prediction. The shape of the prediction is 
                           of the shape (input_height, input_width)
        
        >>> import numpy as np
        >>> c = Classfier()
        >>> c.predict(np.arange(4665600).reshape((1, 4,1080,1080)),
        ...                                  (4, 540, 540), (7, 356, 356))

        
        """
        log = get_logger()
           
        # mirror padding
        log.info("Mirror padding")
        m_pad_y = int(np.ceil((input_shape[1] - output_shape[1]) / 2))
        m_pad_x = int(np.ceil((input_shape[2] - output_shape[2]) / 2))
        data_pad = self._mirror_padding(data, m_pad_x, m_pad_y)
        step_size_y = input_shape[1] - 2 * m_pad_y
        step_size_x = input_shape[2] - 2 * m_pad_x
        
        # zero padding
        log.info("Zero padding.")
        steps_y = int(np.ceil(data.shape[2] / float(step_size_y)))
        steps_x = int(np.ceil(data.shape[3] / float(step_size_x)))
        z_pad_y = steps_y * step_size_y - data.shape[2]
        z_pad_x = steps_x * step_size_x - data.shape[3]
        data_pad = self._zero_padding(data_pad, z_pad_x, z_pad_y)
        
        prediction = np.zeros(data_pad.shape[2:])
        
        log.info("Process the input data partially")
        # classify the input data partially, where each region is as big as 
        # the given input_shape
        for y in range(steps_y):
            for x in range(steps_x):
                # define the region which is processed
                region_y = (y* step_size_y, (y+1)*step_size_y + 2 * m_pad_y)
                region_x = (x* step_size_x, (x+1)*step_size_x + 2 * m_pad_x)
                log.info("Process in y-direction pixels from: %d to %d"
                         % (region_y[0], region_y[1]))
                log.info("Process in x-direction pixels from: %d to %d"
                         % (region_x[0], region_x[1]))
                region = data_pad[:,:,region_y[0]:region_y[1],
                                  region_x[0]:region_x[1]]
                # classify region by caffe
                log.info("Classify defined region by caffe")
                out = self.forward_all(blobs=self.show_output,
                                       **{self.inputs[0]: region})
                log.info(("Write prediction of region to the prediction of "
                          "the whole input at y-positions %d, %d and "
                          "x-positions %d,%d" % (region_y[0]+m_pad_y,
                          region_y[1]-m_pad_y, region_x[0]+m_pad_x,
                          region_x[1]-m_pad_x)))
                prediction[region_y[0]+m_pad_y:region_y[1]-m_pad_y,
                           region_x[0]+m_pad_x:region_x[1]-m_pad_x] = out[
                                    self.show_output[-1]][0].argmax(axis=0)
        log.info("Substract padding from prediction map.")
        valid_map = prediction[m_pad_y:-(m_pad_y+z_pad_y), 
                               m_pad_x:-(m_pad_x+z_pad_x)]
        return valid_map
        
        
    def _zero_padding(self, data, pad_x, pad_y):
        pad_width = ((0,0), (0,0), (0, pad_y), (0, pad_x))
        return np.pad(data, pad_width=pad_width, mode="constant")
    
    def _mirror_padding(self, data, pad_x, pad_y):
        # Due to the convolution some pixels of the input get lost.
        # Border pad describes the number of lost pixels per axes.
        pad_width = ((0,0), (0,0),(pad_y, pad_y), (pad_x, pad_x))
        return np.pad(data, pad_width=pad_width, mode="symmetric")
    