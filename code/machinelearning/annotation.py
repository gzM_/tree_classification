'''
Created on 12.07.2016

@author: mr42
'''
import numpy as np


class Annotation(dict):
    '''
    classdocs
    '''


    def __init__(self, samples, mapper, x_raster, y_raster):
        """Create a annotation.
        
        The annotation contains three pixel maps. Each map is as big as the
        input data(in our case as big as the Raster). In the first map a class
        label is assigned to each pixel. The second map assigns a cost weight
        to each pixel. That means if a pixel with a high weight is classified
        wrong this comes with a high cost. The third map is needed for unet.
        In unet a sub-area of the whole input area is picked randomly and 
        trained afterwards. So the selection weights of the third map
        describe the probability that a pixel is selected. 
        
        :param samples: The samples containing patch and class information.
        :type samples: Samples
        :param raster: The original not normed data.
        :type raster: Raster
        """
        super(Annotation, self).__init__()
        labels = np.zeros((1, y_raster, x_raster),
                          dtype=np.float32)
        weights = np.zeros((1, y_raster, x_raster),
                           dtype=np.float32)
        selection_weights = np.zeros((1, y_raster, x_raster),
                                     dtype=np.float32)
        for s_id, s in samples:
            boundary = []
            for p in s["polygon"]:
                x, y = mapper.world2Pixel(p[0], p[1])
                boundary.append((x,y))
                      
            for x in range(boundary[0][0], boundary[2][0]):
                for y in range(boundary[1][1], boundary[0][1]):
                    selection_weights[0, y, x] = 1
                    weights[0, y, x] = 1
                    labels[0, y, x] = s["BA_id"]                    
  
        self.update({"labels": labels, "weights": weights,
                     "selection_weights": selection_weights})
        