'''
Created on 11.07.2016

@author: mr42
'''
from pandas import DataFrame
from pandas import Series
import pandas as pd
import numpy as np
import copy


class Samples(object):
    '''
    classdocs
    '''


    def __init__(self, df, add_numeric_class_id=True, ignore_mixed=False):
        """Create samples.
        
        :param df: A dataframe containing samples as rows.
        :type df: DataFrame
        
        >>> from pandas import DataFrame
        >>> df = DataFrame({"Id": [1,2,3,4,5,6] ,
        ...                 "BA": ["Ta", "Ki", "Dgl", "Ta", "Dgl", "Ta"]})
        >>> s = Samples(df)
        
        """
        self.data = df
        if ignore_mixed:
            self.data = self.data.drop(np.nonzero(df["BA"] == "M")[0])
        if add_numeric_class_id:
            self.add_numeric_class_id()
        
    def add_numeric_class_id(self):
        """Add a column containing the class ids as numeric values and not as
        strings.

        >>> from pandas import DataFrame
        >>> df = DataFrame({"Id": [1,2,3,4,5,6] ,
        ...                 "BA": ["Ta", "Ki", "Dgl", "Ta", "Dgl", "Ta"]})
        >>> s = Samples(df)
        >>> s.add_numeric_class_id()
        >>> df = DataFrame({"Id": [1,2,3,4,5,6] ,
        ...                 "BA": ["Ta", "Ki", "Dgl", "Ta", "Dgl", "Ta"],
        ...                 "BA_id": [5,4,3,5,3,5]}, index=[0,1,2,3,4,5])
        >>> from pandas.util.testing import assert_frame_equal
        >>> assert_frame_equal(df, s.data.sort_index(axis=1),check_dtype=False)
        
        """
        self.data["BA_id"] = Series(np.zeros(len(self.data["BA"])),
                                    index=self.data.index)
        self.data["frequ"] = Series(np.zeros(len(self.data["BA"])),
                                    index=self.data.index)
        classes = self.data.groupby(by="BA")
        
        for c_id, (c_name, c_content) in enumerate(classes):
            for index, row in c_content.iterrows():
                self.data.ix[index, "BA_id"] = c_id
                self.data.ix[index, "frequ"] = len(c_content)
                
    def split(self):
        """Split the samples into a training and test samples.
        
        :returns: dict A dict containing the training Samples and test Samples.
        
        >>> from pandas import DataFrame
        >>> df = DataFrame({"Id": [1,2,3,4,5,6] ,
        ...                 "BA": ["Ta", "Ki", "Dgl", "Ta", "Dgl", "Ta"]})
        >>> s = Samples(df, add_numeric_class_id=False)
        >>> res = s.split()
        >>> len(res)
        2
        >>> exp = DataFrame({"Id": [3,2,1,4],
        ...                  "BA": ["Dgl", "Ki", "Ta", "Ta"]},
        ...                  index=[2,1,0,3])
        >>> exp.equals(res["train"].data)
        True
        >>> exp = DataFrame({"Id": [5,6],
        ...                  "BA": ["Dgl", "Ta"]},
        ...                  index=[4,5])
        >>> exp.equals(res["test"].data)
        True
        
        """
        tree_classes = self.data.groupby(by="BA")
        df_train = DataFrame()
        df_test = DataFrame()
        for name, group in tree_classes:
            split_point = int(round(len(group)/2.))
            df_train = pd.concat([df_train, group.iloc[0:split_point]])
            df_test = pd.concat([df_test, group.iloc[split_point:]])
        return {"train": Samples(df_train,add_numeric_class_id=False),
                "test": Samples(df_test, add_numeric_class_id=False)}
        
    def to_csv(self, path):
        """Write the samples to a csv file.
        
        Internally just the dataframe containing the samples' information is
        saved as csv.
        
        :param path: The path where to save the csv file.
        :type path: str

        """
        self.data.to_csv(path)
        

    def __iter__(self):
        """Iterate over the samples.
        
        Each sample is a row in a dataframe. So iterating over the samples
        is equivalent to iterating over the rows of the dataframe.
        
        :returns: Iterator The dataframe's row iterator.
        
        """
        return self.data.iterrows()
