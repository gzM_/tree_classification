'''
Created on 21.10.2016

@author: mr42
'''

class Mask(object):
    '''
    classdocs
    '''


    def __init__(self, batches, mapper, extra_border=90):
        '''Create a mask.
        
        :param batches: A dataframe containing the polygons defining
                        the mask. 
        
        :param mapper: The mapper transforming the world coordinates of the
                       polygons into the pixel coordinates of the data. 
        :param extra_border: The number of pixels which should be added as
                             border to the area of the polygons. This is 
                             needed as perceptive field for unet.                   
        
        '''
        self.polygons = [None] * len(batches)
        for b_id, b in batches.iterrows():
            boundary = []
            for p in b["polygon"]:
                x, y = mapper.world2Pixel(p[0], p[1])
                boundary.append((x,y))
            region_y = [boundary[1][1] - extra_border,
                        boundary[0][1] + extra_border]
            region_x = [boundary[0][0] - extra_border,
                        boundary[2][0] + extra_border]
            if region_y[0] < 0:
                region_y[0] = 0
            if region_x[0] < 0:
                region_x[0] = 0
            self.polygons[b["id"]] = (region_x, region_y)  
            
    def __len__(self):
        return self.polygons.__len__()
            
    def __iter__(self):
        return self.polygons.__iter__()
    
    def __getitem__(self, key):
        return self.polygons[key]
        
    def get_masked_area(self, data):
        '''Get the area of data which has overlaps with the mask.
        
        Iterate over the polygons of the mask. For each polygon extract the data
        which is inside of the polygon. In the end the extracted data of all 
        polygons is returned as list.   
        
        :param data: The data as numpy array.
        :returns A list of numpy array where each array contains the data
                 of a area described by one of the mask's polygon.
        
        '''
        result = []
        for region_x, region_y in self:
            result.append(data[:, region_y[0]:region_y[1],
                               region_x[0]:region_x[1]])
        return result
