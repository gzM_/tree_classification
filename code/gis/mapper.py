'''
Created on 20.10.2016

@author: mr42
'''

class Mapper(object):
    '''
    classdocs
    '''


    def __init__(self, geo_trans):
        '''
        Constructor
        '''
          # coordinates of the upper left corner.
        self.ulX = geo_trans[0]
        self.ulY = geo_trans[3]
        # geometric distance of a pixel
        self.xDist = abs(geo_trans[1])
        self.yDist = abs(geo_trans[5])
        
    
    def world2Pixel(self, x, y):
        """Compute the pixel location of a geospatial coordinate 
        
        First compute the difference between the given world coordinate
        and the upper left corner world coordinate. Afterwards this distance
        is divided by the world distance of one pixel(in our case this is
        0.1 meters per pixel).
        
        :param x: The x position in the world.
        :type x: float
        :param y: The y position in the world.
        :type y: float
        :returns: tuple The pixel coordinates as tuple of x- and y-position.
        """
        x_pos = int(round((x - self.ulX) / self.xDist))
        y_pos = int(round((self.ulY - y) / self.yDist))
        return (x_pos, y_pos)
        