'''
Created on 19.05.2016

@author: mr42
'''
from osgeo import gdal, gdalnumeric
import numpy as np
import scipy
import gc


class RasterReader(object):
    '''
    classdocs
    '''

    def get_data(self, path, norm=True, xoff=0, yoff=0, xsize=None, ysize=None):
        """Load the raster file as numpy array.
        
        :param path: The path to the raster file.
        :type path: str
        :param norm: If true the data is normalized between 0 and 1.
        :type norm: boolean
        """
        # Load raster as uint16 array
        data = gdalnumeric.LoadFile(path, xoff=xoff, yoff=yoff,
                                    xsize=xsize, ysize=ysize)
        data = data.astype(np.float32, copy=False)

        # replace nan value of gis which is -3.40282e+38 by 0
        nan_in_gis = data.min()
        data[data==nan_in_gis] = 0
        # normalize the data between 0 and 1
        if norm:
            data = self.normalize(data)
        return data
    
    def get_geo_trans(self, path):
        """Load the raster file and extract the geometric information. 
        
        :param path: The path to the raster file.
        :type path: str

        """
        # Get the Geo information of the raster file.
        srcImage = gdal.Open(path)
        return srcImage.GetGeoTransform()

    def normalize(self, data):
        """Normalize the given array by normalize the data between 0 and 1.
        
        :param data: The data to normalize
        :type data: np.ndarray
        
        """
        # if data has multiple channels. Normalize channel vice.
        if len(data.shape) == 3:
            for c in range(data.shape[0]):
                np.subtract(data[c], data[c].min(), data[c])
                np.divide(data[c], data[c].max(), data[c])
        else:
            data /= data.max()
        return data
        