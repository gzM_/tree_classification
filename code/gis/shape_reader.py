'''
Created on 19.05.2016

@author: mr42
'''
from osgeo import gdal, gdalnumeric, ogr, osr
from pandas import DataFrame


class ShapeReader(object):
    '''
    classdocs
    '''


    def get_data(self, path, parse_geometry=True):
        """Collect the shapes from the given .shp file as dataframe rows.
    
        Each row contains first the shape's
        attributes(e.g. class definition) and second
        the polygon describing the shape's geometric representation.
        The polygon is a list of points.
        Each point is a tuple containing the x and y coordinate.
        
        :param path: The path to the .shp file. 
        :type path: str
        :param geometry: Defines if the geometry objects of gis are parsed
                         into python lists and tuples.
        :type geometry: boolean
        """
        shapef = ogr.Open(path)
        lyr = shapef.GetLayer()
        fields = lyr.GetLayerDefn()
        # Get the fieldnames which are the keys to extract the attributes later
        fieldnames = []
        cols = {}
        for i in range(fields.GetFieldCount()):
            fieldnames.append(fields.GetFieldDefn(i).GetName())
            cols[fields.GetFieldDefn(i).GetName()] = []
        
        if parse_geometry:
            cols["polygon"] = []
        else:
            cols["wkt_geom"] = []
        
        for f in lyr:
            geom= f.geometry()
            if parse_geometry:
                cols["polygon"].append(geom.Boundary().GetPoints())
            else:
                cols["wkt_geom"].append(str(geom))
            for att in fieldnames:
                cols[att].append(f[att])

        return DataFrame(cols)
