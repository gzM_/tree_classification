#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 16.09.2014

@author: mrudolph
'''

import multiprocessing as mp
import dill
import gc
import inspect
import sys
from log import get_logger
import traceback
import signal


# make traceback objects pickable

# Extension of the classes of the tblib 1.0.1, so that also
# exceptions raised by functions with kwargs can be handled.
# If tblib is updated, this code may have to be edited.
import tblib.pickling_support
tblib.pickling_support.install()


# Wrapper for exceptions which occurred in a subprocess. The wrapped exceptions are re-raised
# later in the parent process. The code of this class is partly copied from
# http://stackoverflow.com/questions/6126007/python-getting-a-traceback-from-a-multiprocessing-process
class PickableException(object):

    def __init__(self, task_id, kwargs):
        """Set the type, message and traceback of the exception given by sys.exc_info.
        """
        self.type,  m, self.tb = sys.exc_info()
        self.m = "An error occurred in the task %d and the message is: %s" % (task_id, m)

    def re_raise(self):
        """Raise the wrapped exception.
        """
        # improves it somewhat, but doesn't solve it. Even the following line doesn't work
#         raise self.type, 'A simpple\nerror message', self.tb
        raise self.type, self.m, self.tb
    
    def log(self, tb_offset=None):
        log = get_logger()
        tb_lines = traceback.format_tb(self.tb)
        if tb_offset is not None:
            tb_lines = tb_lines[tb_offset:]
        log.error(''.join(tb_lines))
        log.error('{0}: {1}'.format(self.type, self.m))


def _receive_and_execute_task(tasks, done, terminate_hard):
    """Every subprocess calls this method once, to look continuously for a remaining task to execute.

    If a subprocess has nothing to do, it tries to execute the next remaining task in the
    task queue until it reads a 'STOP'.

    Parameters
    ----------
    tasks : multiprocessing.Queue
            The tasks which are processed by the subprocesses.
    done : multiprocessing.Queue
           A queue for storing the results of the tasks.
    log_queue : multiprocessing.Queue
                A queue for storing the logging statements of all processes.
    """

    # In the moment only the root logger is initialized by the root logger of the parent process.
    # look for a remaining task until reading 'STOP'
    for task_id, task, args, kwargs in iter(tasks.get, 'STOP'):
        __execute_task(task_id, dill.loads(task), dill.loads(args),
                       dill.loads(kwargs), done, terminate_hard)


def __execute_task(task_id, task, args, kwargs, done, terminate_hard):
    """Execute the given task and store the result in the done queue.

    Try to execute the task. If it finishes successfully put the result into the done queue.
    If it terminates unusually, raise the Exception and put an error message as result into
    the done queue.

    Parameters
    ----------
    task_id : int
              The id of the task.

    task : function
           The task to perform in form of a function.

    args : argument list
           The arguments for the task.

    done : multiprocessing.Queue
           A queue for storing the result of the task.

    """
    try:
        # Normally the imports must be placed inside the function task, since the module wide import
        # does not work if dill is used for serializing the function.
        # The following lines solve this problem by adding all imports of the function's module
        # to the globals of the function.
        mod = inspect.getmodule(task)
        for m in inspect.getmembers(mod):
            task.__globals__.update({m[0]: m[1]})
        task_result = task(*args, **kwargs)
        # put the result of the task into the done queue.
        done.put((task_id, dill.dumps(task_result)))
    except:
        # -1 cause of the case 0
        pe = PickableException(task_id, kwargs)
        # if terminate_hard is false, log the exception, so that the user is informed that this 
        # process raises an error. This is important since multiple processes can raise an 
        # error and only the last error is thrown.
        if not terminate_hard:
            pe.log()
        # check if the type of of the exception is pickable. If not replace type by Exception class
        # type.
        try:
            done.put((-task_id - 1, dill.dumps(pe)))
        except:
            pe.type = Exception
            try:
            # if the exception is still not pickable replace the traceback by None
                done.put((-task_id - 1, dill.dumps(pe)))
            except:
                pe.tb = None
                done.put((-task_id - 1, dill.dumps(pe)))
    # call the garbage collector to remove free unneeded memory.
    gc.collect()



class ProcessManager(object):

    def terminate(self):
        for proc in self._processes:
            # kill a process if it is not killed already
            log = get_logger()
            log.info("Terminating all processes.")
            if not (proc.exitcode is not None and proc.exitcode != 0):
                proc.terminate()

    def __init__(self):
        """Initialize an process manager for multiprocessing.
        """
        self.__tasks = mp.Queue()
        self.__id = 0
        self._processes = []

    @property
    def processes(self):
        """Returns the list of the subprocesses.

        Returns
        -------
        return : list
                 The list of the running subprocesses.

        """
        return self._processes

    def add_task(self, func, args=(), **kwargs):
        """Add a task.

        Parameters
        ----------
        func : function
               The function which performs the task.
        args : argument list
               The arguments for the given function in the form (arg1, ...,argn).
        kwargs : dict
                 Additional keyword arguments.
        """
        self.__tasks.put((self.__id, dill.dumps(func), dill.dumps(args), dill.dumps(kwargs)))
        self.__id += 1

    def __reset(self, nproc):
        """Reset all member variables to their initial value and stop all processes.

        Parameters
        ----------
        nproc : int
                Number of processes.

        """
        # Send a stop signal to all subprocesses.
        for i in range(nproc):
            self.__tasks.put('STOP')
        # reset member variables
        self.__id = 0
        self.__tasks = mp.Queue()
        # non - serialize reset routine
        # reset process list.
        self._processes = []

    def __get_data(self, ntasks, done_queue, terminate_hard):
        """Get the data from the done queue and put it into a list.

        For every task an result exists on the done queue. This result is either a value or None or
        an error message. So for every task the get method of the done queue is called.
        This method waits until a result is put on the queue by a subprocess. If that's the case
        it returns the result together with the task id. If the task id is positive no error
        has occurred and the result of the task is stored in the result list.
        In case of an error the id of the bugged task is stored as error id. If
        terminate_hard is true, all other subprocesses are terminated, otherwise the other
        subprocesses finish their jobs as usually.
        In the end the tuple consisting of the error id and and the result list is returned. The
        error id is None, if no error has occured.

        Parameters
        ----------
        ntasks : int
                 The number of tasks.

        done_queue : multiprocessing.Queue
                     The queue which contains the results of all tasks.

        terminate_hard : bool
                         If an error occurs and terminate_hard is true all other subprocesses
                         are terminated, otherwise they finish their jobs as usually.

        Returns
        -------
        result : tuple
                 The tuple (error, result) is returned, where error is usually None and result
                 contains the task results received from the done queue. Only if an error occurs,
                 the value of the error variable is the id of the task which produces the error.

        """
        result = []
        error = None
        for i in range(ntasks):
            # receive task result
            taskid, taskresult = done_queue.get()
            # error handling
            if taskid < 0:
                # -1 cause we subtract -1 in execute task to handle also the case 0.
                error = taskresult
                if terminate_hard:
                    # give some time to the subprocess to raise its error
                    #time.sleep(1)
                    # terminate all subprocesses and stop collecting task results.
                    self.terminate()
                    break
            # fill result list
            result.append((taskid, taskresult))
        return (error, result)

    def execute(self, nproc=None, terminate_hard=True):
        """Assign the tasks to multiple processes and execute them.

        Parameters
        ----------
        nproc : int, default None
               Number of processes. Per default the number
               of processes is equal to the number of cores, except
               there are less tasks than cores. In this case the
               number of processes is equal to the number of tasks.
        terminate_hard : bool, default True
                         If an error occurs and terminate_hard is true all other subprocesses
                         are terminated, otherwise they finish their jobs as usually.
        logging : boolean, default True
                  If multiple processes write log statements to one stream this can lead to cryptic
                  messages(stream=Console) or access problems(stream=File). To avoid this set
                  logging to true since then one thread is responsible to synchronize the log
                  messages.

        """
        # All subprocesses inherit sigint handler of main process
        original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        signal.signal(signal.SIGINT, original_sigint_handler)
        
        # The logging code is partly copied from https://gist.github.com/schlamar/7003737
        # The processes write their log statements to the log queue.
        if nproc is None:
            nproc = mp.cpu_count()

        # import the most recent cfg version which contains all current modifications. This
        # modified version is passed to all subprocesses.

        done_queue = mp.Queue()

        # start not more processes than there are tasks
        if self.__id < nproc:
            nproc = self.__id

        # disable exceptions and warnings for time of pickling, since the overridden sys.excepthook
        # is not pickable
        try:
            for i in range(nproc):
                proc = mp.Process(target=_receive_and_execute_task,
                                  args=(self.__tasks, done_queue, terminate_hard))# logpy.Logger.manager.loggerDict))
                self._processes.append(proc)
                proc.start()
    
            
            # Get result
            error, result = self.__get_data(self.__id, done_queue, terminate_hard)
            # Reset
            self.__reset(nproc)
            # Handle error
            if error is not None:
                error = dill.loads(error)
                error.re_raise()
    
            # Sort tasks by the order they were added.
            result.sort()
            # if you run in a terminal first a script which imports parallel and then a script which 
            # runs this method the program crashes. The reason is that the _main_module of dill.dill
            # is not updated so it is ?. Because of that the line
            # if type(obj).__module__ == _main_module.__name__: causes a crash, since _main_module
            # has no attribute __main__. To avoid this crash we update the _main_module here.
            import __main__ as m
            dill.dill._main_module = m
            result = [dill.loads(t[1]) for t in result]
    
            return result
            
            # stop the while true loop of the logging thread by putting a None into the log_queue
        except KeyboardInterrupt:
            self.terminate()
            raise KeyboardInterrupt()

        