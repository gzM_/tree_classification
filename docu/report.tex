\documentclass[11pt,twocolumn]{article}

\usepackage[utf8]{inputenc}

%test commit

\usepackage[T1]{fontenc}

\usepackage{cite}

\usepackage{tikz}
\usetikzlibrary{calc}
\usetikzlibrary{fit}

\usepackage[autostyle=true,german=quotes]{csquotes}

%\usepackage{babel}
\usepackage{graphicx}

\usepackage{algorithmic}
%\usepackage[english, lined]{algorithm2e}
\usepackage[english, ruled, noend]{algorithm2e}
\usepackage{amsmath}

\usepackage[switch]{lineno}
\usepackage{amssymb}
\usepackage{subfigure}
\usepackage{comment}
 \usepackage{url}
\usepackage{color}
%\usepackage{amsthm}

% add legend to graphs
\usepackage{pgfplots}

% argument #1: any options
\newenvironment{customlegend}[1][]{%
    \begingroup
    % inits/clears the lists (which might be populated from previous
    % axes):
    \csname pgfplots@init@cleared@structures\endcsname
    \pgfplotsset{#1}%
}{%
    % draws the legend:
    \csname pgfplots@createlegend\endcsname
    \endgroup
}%

% makes \addlegendimage available (typically only available within an
% axis environment):
\def\addlegendimage{\csname pgfplots@addlegendimage\endcsname}

\author{Michael Rudolph}

\title{Analyse von Dokumentation und Kommunikation von Aspekten der Nachhaltigkeit durch Unternehmen im Internet.}
% 
\newtheorem{name}{Printed output}

\newtheorem{mydef}{Definition}

\newcommand{\note}[1]{{(Note: #1)}}

%glossary
\usepackage{glossaries}

\newglossaryentry{dtm}% the label
{name={DTM},% the term
 description={Digital Terrain Model },% brief description
plural={DTMs}% the plural
}

\newglossaryentry{ndsm}% the label
{name={NDSM},% the term
 description={Normalized Digital Surface Model },% brief description
 plural={NDSMs}% the plural
}

\newglossaryentry{relu}% the label
{name={ReLU},% the term
 description={Rectified Linear Unit},% brief description
 plural={ReLUs}% the plural
}

\newglossaryentry{blob}% the label
{name={BLOB},% the term
 description={Binary Large Object},% brief description
 plural={BLOBs}% the plural
}

\newglossaryentry{hdf5}% the label
{name={HDF5},% the term
 description={Binary Large Object},% brief description
 %plural={BLOBs}% the plural
}

\newglossaryentry{unet}% the label
{name={U-net},% the term
 description={Neural Network U-net, described in \cite{unet} },% brief description
}

\newglossaryentry{rgb}% the label
{name={RGB},% the term
 description={RGB denotes a 3-channel image consisting of a red, green, and blue channel },% brief description
}


\makeglossaries




\begin{document}
%\linenumbers
\input{titlePage/titlepage}

%\maketitle

\section*{Acknowledgements}

I am grateful to Prof. Dr. Brox and Dr. Adler for that they have given me the
possibility to do this project.
Additionally, I am thankful that Dr. Dreiser provides us the data of the national park \enquote{Schwarzwald}.
Last but not least, I give thanks to my supervisor Dominc Mai for his great support.

%\tableofcontents

\section{Introduction}
The demand for wood is great. There is an ongoing competition between the paper, furniture and energy industry. Besides the product wood forests offer a biosphere for humans and animals. Due to these reasons the control of the forest stand is unalterable. Since inventories are time consuming and costly the demand for applications which are capable to detect tree species automatically is high.

However, the task is quite challenging. Trees of one specie can differ in height, width, treetop and so forth. Additionally, the leaves appearance and reflection depends on the lighting conditions and the season. Due to this wealth of variants techniques of the field of machine learning are necessary. 
In \cite{selina_thesis} support vector machines are trained with hand-crafted features. As data basis aerial photographs of the national park \enquote{Schwarzwald} taken in April 2015 and July 2014 are used. Additionally, a \glsdesc{dtm}(\gls{dtm}) of 2003 is given. 
First, point clouds are created by applying image matching techniques to the aerial views. On the basis of the point clouds and the DTM an orthophoto and a \glsdesc{ndsm}(\gls{ndsm}) is computed for each aerial photograph. Secondly, different features are extracted.
As annotations 2x2 meter patches
are labelled with a tree specie. 
The trained support vector machines are capable to classify the 2x2 meter patches into tree species with an overall accuracy of 90 percent in the cross validation and 75 percent in the validation.

Besides the great results the described approach has some  drawbacks. It ignores the relation of neighbouring trees by only looking at each patch independently. Additionally, the resolution of the classification is bounded to 2x2 meter. Meaning, it is not possible to distinguish two different tree species which fall into one patch. 
Another disadvantage is that expert knowledge is needed to create reasonable features. 

Together with the forest institute of Baden-Wuerttemberg and the authors of \cite{selina_thesis} we present an approach based on a Neural Network which addresses the  mentioned drawbacks. Besides the orthophotos and the \glspl{ndsm} no extra features are needed, since the Neural Network  learns jointly the features and the classification.
Given the annotations of \cite{selina_thesis} we create a pixel wise annotation map. Meaning, all pixels belonging to a patch are marked with the patch label. As result the Neural Network outputs a classification on the pixel level which is not limited to any grid resolution. When assigning a label to a pixel the Neural Network also considers the neighbouring pixels. Thereby, our approach can model the relation between nearby trees better than the patch based one. In the next sections we first describe the data basis in more detail. In the next step we explain the training phase. Afterwards the data is classified by the network into tree species. Finally, we illustrate the results. 



\section{Data}

\begin{figure*}
  \begin{center}
  \subfigure[\gls{rgb} values of the orthophoto. ]{
  \includegraphics[trim=0 0 30cm 0, scale=0.16, clip]{media/rgb.png}
  \label{fig:rgb_channels}}
\subfigure[Infrared values of the orthophoto.] {
   \includegraphics[trim=0 1cm 27cm 0, scale=0.17, clip]{media/infrared.png}
  \label{fig:infrared_channel} }
 \subfigure[Vegetation height of the \gls{ndsm}.] {
  \includegraphics[trim=0 1cm 27cm 0, scale=0.17, clip]{media/height.png}
   \label{fig:height_channel}}
  \caption{(a)-(b) illustrate the different channels of an orthophoto showing a part of the national park. In (c) the values of the corresponding \gls{ndsm} are visualized. The values of each channel are normalized between 0 and 1.}
  \end{center}
\end{figure*} 

In our approach we use the data provided by the authors of \cite{selina_thesis} to train a Neural Network to learn a per pixel classification of tree species.
The data consists of orthophotos and \glspl{ndsm} of the national park \enquote{Schwarzwald} taken from April and July. Both the orthophotos and the \glspl{ndsm} are given as raster files, where 1 meter is represented by 10 pixels. In a first step the data is normalized channel wise between zero and one. Figure \ref{fig:rgb_channels}, Figure \ref{fig:infrared_channel} and Figure \ref{fig:height_channel} illustrate the different channels of the normalized data.  
Figure \ref{fig:rgb_channels} shows the \gls{rgb} values of a forest trail surrounded by some trees. The black areas arise from the orthorectification. In our approach we reach good results by setting the pixel values of these areas to zero in all channels. Figure \ref{fig:infrared_channel} illustrates the infrared values of the corresponding \gls{rgb} values. The vegetation height is shown in Figure \ref{fig:height_channel}. 
After the normalization the five channels are merged. This results in a matrix with the dimension $5 \times height_{nationalpark} \times width_{nationalpark}$.



\begin{figure*}
  \begin{center}
  \subfigure[2x2m labels of \cite{selina_thesis}. ]{
  \includegraphics[scale=0.12]{media/original_train_test_shapes.png}
  \label{fig:train_shapes}}
\subfigure[Point labels are the result of the tree inventory.] {
   \includegraphics[scale=0.12]{media/original_validation_shapes.png}
  \label{fig:val_shape} }
  \subfigure[The cells containing the points of (b) are marked with the point color.] {
   \includegraphics[scale=0.12]{media/original_validation_shapes_rect.png}
  \label{fig:val_shape_rect} }
  \caption{(a)-(c) show the \gls{rgb} data of different parts of a orthophoto. Additionally, a 2x2m grid is illustrated in white. The tree labels of spruce and pine are shown in blue and red respectively.}
  \end{center}
\end{figure*}




Besides the orthophotos and \glspl{ndsm} the tree species labels are provided in two files. The first file contains the labels created by the authors of \cite{selina_thesis} whereas the labels of the second file were collected during a tree inventory.   
The authors of \cite{selina_thesis} laid a grid over the national park with the resolution 2x2m which corresponds to $20x20$ pixels. In the next step some cells were selected and marked with the corresponding tree label. Figure \ref{fig:train_shapes} shows a small part of the national park. The blue cells and red cells represent spruce and pine respectively.
In the tree inventory the trees species were marked with a point like in Figure \ref{fig:val_shape}. To get a uniform labelling we first span the 2x2m grid over the point labels. Secondly, the cells containing a point are marked with the label of the point. Applying these steps to the point labels of Figure \ref{fig:val_shape} results in the labels of Figure \ref{fig:val_shape_rect}. 
So we end up with two files which contain 2x2m patches which are marked with tree labels. The patches of the class \enquote{Mischpixel} are deleted because they are not consistent with the other labels since they are assigned to pixels of all tree species. 
To distinguish background like forest trails or houses from trees we add to the existing files 2x2m patches describing background. 
In the last step the tree labels are split into a training, test and validation set. For the test set we select randomly 10 percent of the labels of each tree species of the first vector file. The remaining labels are used for the training. 
As validation set the labels of the tree inventory are used.
The Table \ref{tab:sample_distribution} illustrates the distribution of the tree species of the different sets. 
In the training and test set the tree species are equally spread whereas in the validation set some tree species like spruce are over- and some like broadleaf are under-represented. 

\begin{table}
\begin{center}
\begin{tabular}{|p{1.8cm}|p{1.5cm}|p{1.3cm}|p{1.75cm}|}
\hline
Species&2x2m Patches Training&2x2m Patches Testing&2x2m Patches Validation\\
\hline
Background (Bgr)&173 &19 &15 \\ \hline
Douglas-fir(Dgl)&135 &14 &4 \\ \hline
Spruce(Spr)&177 &19 &334 \\ \hline
Pine(Pi)&182 &20 &175 \\ \hline
Broadleaf (Bl)&137 &15 &4 \\ \hline
Larch(La)&140 &15 &18 \\ \hline
Fir&183 &20 &31 \\ \hline
\end{tabular}
\caption{The tree species distribution of the training, test and validation set. }
\label{tab:sample_distribution}
\end{center}
\end{table}



\section{Network Architecture}

\begin{figure*}
  \begin{center}
  \includegraphics[page=2,trim=5cm 3cm 2cm 4cm, scale=0.75, clip]{media/net.pdf}
  \caption{The gray boxes are the layers of the network \gls{unet}. Above each layer its channel size is denoted. The layer dimension is given on the left side of the gray boxes. Between the layers the applied operations are visualized by arrows. }
  \label{fig:network}
  \end{center}
\end{figure*}
Using the framework \enquote{caffe}\cite{caffe} we train a fully convolutional neural network based on the \gls{unet}\cite{unet} architecture to learn a per pixel classification of tree species.
Figure \ref{fig:network} illustrates the u-shaped network architecture of \gls{unet}. It consists of an analysis and synthesis part. The first layer of the analysis part is the input layer. It requires the tree data described in the previous section. It is a 5-channel image containing the normalized \gls{rgb}, infra red and vegetation height values. 
Given the input, 3x3 convolutions, each followed by a \glsdesc{relu}(\gls{relu}) and max-pooling operations are applied repeatedly to extract descriptive feature maps. The synthesis part consists of a chain of 2x2 up-samplings and 3x3 convolutions with a following \gls{relu}. After each up-sampling the remaining features are concatenated with the cropped layers of the analysis part to provide the features of the higher resolution. Finally, a 1x1 convolution maps the 128x324x324 feature matrix to a 7x324x324 one. Each channel contains the pixel wise probability of one tree species. Picking the tree species with the highest probability results in a 324x324 matrix containing the predicted tree species.

Due to the memory limit of 6GB of the nvidia TITAN GPU the input dimension of the network is only 508x508. For a easier handling we first split the national park into sub-areas of the dimension 3090x5090 which are shown in Figure \ref{fig:subareas}. A sub-area is chosen such that it contains as many tree species labels as possible.
In the next step each sub-area is predicted by applying the overlap-tile strategy described in \cite{unet}.
So we end up with a 3090x5090 pixel map of tree species for each sub-area. This partitioning is done for both orthophotos taken from April and July.




\begin{figure}
  \begin{center}
  \includegraphics[scale=0.25]{media/batches.png}
  \caption{The green area shows the contour of the national park \enquote{Schwarzwald}. This area is divided into the blue coloured sub-areas. Each sub-area contains some tree species labels which are marked in white.}
  \label{fig:subareas}
  \end{center}
\end{figure}

\section{Training}
\begin{figure}
  \begin{center}
  \includegraphics[trim=0 2cm 32cm 0, scale=0.2, clip]{media/loss.png}
  \caption{The blue line shows the loss over the course of time. Furthermore, the loss average is illustrated in green. For the computation of the average a window size of 41 is used.}
  \label{fig:loss}
  \end{center}
\end{figure}

\begin{figure*}
  \begin{center}
  \subfigure[The pixel labels.]{
  \includegraphics[trim=0 0 17cm 0, scale=0.17, clip]{media/labels.png}
  \label{fig:labels}}
\subfigure[The weight and selection probability of the labelled pixels of (a) is set to 1.] {
   \includegraphics[trim=0 2cm 27cm 0, scale=0.19, clip]{media/weights.png}
  \label{fig:weights_probability} }
  \caption{(a) shows the labels and (b) the weights and selection probability of some pixels.}
  \end{center}
\end{figure*} 

In the training phase the network requires in addition to the tree data from April and July the tree species labels. At first, a matrix with the dimension of the tree data is initialized with zeros. We assign a number between 1 and 6 to each tree specie. The background class is set to zero.
In the next step the pixels belonging to a 2x2m patch are labelled with the number of the tree specie of the patch. Figure \ref{fig:labels} illustrates the resulting pixel labels. The number of pixels of one patch can be marginally greater than $20x20$ if the border of the patch shape differs from the border of the pixels.

In addition to the labels \gls{unet} offers the possibility to give some pixels more importance in the training using a weight map. In our approach we set the weights of the labelled pixels to one and all others to zero.



\begin{table}
\begin{center}
\begin{tabular}{|c|c|}
\hline
Learning rate& 0.00001\\ \hline 
Learning policy& fixed\\ \hline 
Momentum 1 & 0.9\\ \hline
Momentum 2 & 0.99\\ \hline
Iterations & 105000 \\ \hline
Batch size & 1 \\ \hline
Solver type & Adam \\ \hline
\end{tabular}
\caption{The hyper-parameters used during the training phase. }
\label{tab:hyperparameters}
\end{center}
\end{table}

In each iteration step the network randomly selects a 508x508 pixel area to train on. It is possible to define a sampling weight for each pixel. This allows the calculation of a pseudo-probability for each pixel by dividing the sampling weight of the pixel by the sum of all sampling weights. The center of the 508x508 pixel area is set according to the pseudo-probabilities. To focus the training on the labelled areas we set the sampling weight of the marked pixels to one and to zero for all others. Consequently, the probability of all marked pixels to be the center of the 508x508 pixel area is the same. Figure \ref{fig:weights_probability} visualizes both the weights and selection probilities of the pixels of Figure \ref{fig:labels}.



Given the tree data, labels, weights and selection probabilities, we train the network with the hyper-parameters shown in Table \ref{tab:hyperparameters}. 
After around 100000 iterations the loss of the network converges like illustrated in Figure \ref{fig:loss}. %0.15. 
Since the training labels are relatively sparse we use rotation as data augmentation. Like shown in \cite{dataAugmentation}, data augmentation also helps to add invariance against the respective transformations of the data.
\begin{table*}
\begin{center}
\begin{tabular}{|p{1.5cm}|p{1.35cm}|p{0.9cm}|p{1.4cm}|p{1.35cm}|p{0.9cm}|p{1.4cm}|p{1.35cm}|p{0.9cm}|p{1.4cm}|}
\hline
Species&Precision Train & Recall Train& Accuracy Train & Precision Test & Recall Test& Accuracy Test & Precision Val. & Recall Val.& Accuracy Val.\\
\hline
Back. &0.99 & 0.998 &0.999 & 1 & 0.997 & 0.999 & 0.997 & 0.94 & 0.998\\ \hline
Douglas-fir&0.95 &0.896 & 0.98 & 0.92 & 0.96 & 0.99 & 0 & 0 & 0.95\\ \hline
Spruce&0.93 &0.89 &0.97 & 0.86 & 0.67 & 0.93 & 0.84 & 0.53 & 0.67\\ \hline
Pine&0.93 &0.95 &0.98 & 0.85 & 0.85 & 0.95 & 0.73 & 0.58 & 0.81\\ \hline
Broadleaf&0.94 &0.92 &0.98 & 0.96 & 0.92 & 0.99 & 0.08 & 0.89 & 0.93\\ \hline
Larch&0.98 &0.9 &0.99 & 0.98 &0.79 & 0.97 & 0.35 & 0.67 & 0.95\\ \hline
Fir&0.86 &0.99 &0.97 & 0.73 & 0.99 & 0.94 & 0.19 & 0.68 & 0.83\\ \hline
\end{tabular}
\caption{Precision, recall and accuracy measured on the training, test and validation set.}
\label{tab:pre_rec_acc}
\end{center}
\end{table*}
\begin{figure}
{\footnotesize 
\begin{align}
true \ pos_{A} =& ground \ truth \ pixels_{A}  \label{eq:first}\\\nonumber & \cap \ predicted \ pixels_{A}  \\\nonumber \\
true \ neg_{A} =& ground \ truth \ pixels_{\neg A} \\\nonumber & \cap \ predicted \ pixels_{\neg A} \\\nonumber \\
false \ pos_{A} =& ground \ truth \ pixels_{\neg A} \\\nonumber & \cap \ predicted \ pixels_{A} \\\nonumber \\
false \ neg_{A} =& ground \ truth \ pixels_{A} \\\nonumber & \cap \ predicted \ pixels_{\neg A} \\\nonumber \\
total \ population =& ground \ truth \ pixels_{\neg A}  \\\nonumber  & \cup \ ground \ truth \ pixels_{A} \\\nonumber \\
precision_{A} =& \frac{true \ pos_{A}}{predicted \ pixels_{A}} \\\nonumber \\
recall_{A} =& \frac{true \ pos_{A}}{ground \ truth \ pixels_{A}} \\\nonumber \\
accuracy_{A} =& \frac{true \ pos_{A} + true \ neg_{A}}{total \ population_{A}} \label{eq:last}
\end{align}}
\end{figure}


\section{Results}
In the experiments we evaluate the precision, recall and accuracy for each tree species on the training, test and validation set which has been described in section 2. The metrics are computed on the basis of the Equations \ref{eq:first} to \ref{eq:last} where A is the currently regarded tree specie. Table \ref{tab:pre_rec_acc} shows the results for these metrics. Additionally, the confusion matrices of the training, test and validation set are illustrated in Figure \ref{fig:cm}. The rows represent the actual tree species whereas the  columns represent the predicted tree species. Furthermore, Table \ref{tab:sample_distribution_pixel} shows the tree species distribution for each set measured in the number of pixels.
All results are computed for both months April and July together.
In the following these results are described and analysed.

For the training set the results are quite good with precision, recall and accuracy values around $0.9$. Even the values of the diagonal of the confusion matrix shown in Figure \ref{fig:cm_training} are close to 100 percent.

The results on the test set are also promising with precision and recall values between $0.7$ and $0.9$. Closer inspection shows that the tree species fir and spruce perform slightly worse in terms of precision and recall respectively.
The reason for this is that spruce is recognized with $23.1$ percent as fir like illustrated by the confusion matrix of Figure \ref{fig:cm_testing}.

\begin{table}
\begin{center}
\begin{tabular}{|p{1.8cm}|p{1.5cm}|p{1.4cm}|p{1.75cm}|}
\hline
Species&Number of Pixels Training&Number of Pixels Testing&Number of Pixels Validation\\
\hline
Background (Bgr)&141 K &16 K &12 K \\ \hline
Douglas-fir(Dgl)&108 K &11 K &3 K \\ \hline
Spruce(Spr)&142 K &16 K &267 K \\ \hline
Pine(Pi)&149 K &16 K &140 K \\ \hline
Broadleaf (Bl)&110 K &12 K &3 K \\ \hline
Larch(La)&112 K &12 K &14 K \\ \hline
Fir&146 K &16 K &25 K \\ \hline
\end{tabular}
\caption{The tree species distribution of the training, test and validation set measured in the number of pixels rounded to the nearest thousand(K). Since we train and predict the tree data from April and July together the number of pixels of both months is shown. }
\label{tab:sample_distribution_pixel}
\end{center}
\end{table}

\begin{figure}
  \begin{center}
  \subfigure[Training confusion matrix.]{
  \includegraphics[trim=16cm 0 5cm 0, scale=0.17, clip]{media/cm_training.png}
  \label{fig:cm_training}}  
\subfigure[Test confusion matrix.] {
   \includegraphics[trim=16cm 0 5cm 0, scale=0.17, clip]{media/cm_testing.png}
  \label{fig:cm_testing} } 
	\subfigure[Validation confusion matrix.]{
   \includegraphics[trim=16cm 0 5cm 0, scale=0.17, clip]{media/cm_validation.png}
  \label{fig:cm_validation} }
  \caption{The confusion matrices.}
  \label{fig:cm}
  \end{center}
\end{figure} 

\begin{figure*}
  \begin{center}
\subfigure[Validation confusion matrix of our approach. ]{
   \includegraphics[trim=16cm 0 5cm 0, scale=0.17, clip]{media/cm_validation.png}
  \label{fig:cm_validation_comp} }  
  \subfigure[Validation confusion matrix of \cite{selina_thesis}. ]{
  \includegraphics[trim=16cm 0 5cm 0, scale=0.17, clip]{media/cm_selina.png}
  \label{fig:cm_selina}}   
  \caption{Comparison between the validation confusion matrix of our approach and \cite{selina_thesis}. In our approach we add the class background to distinguish roads from trees. That's why the confusion matrix of our approach contains the class background whereas the one of \cite{selina_thesis} not.}
  \label{fig:cm_comparison}
  \end{center}
\end{figure*}

The results on the validation set are influenced by the uneven species distribution. This becomes clear when looking at the tree specie broadleaf. 
Although the confusion matrix in Figure \ref{fig:cm_validation} shows that $88.7$ percent of the $3 K$ broadleaf pixels are classified correctly the precision is only $0.08$.
The problem is that the $88.7$ percent of $3 K$ correct classified pixels are relatively few compared to the, for instance, $6.2$ percent of the $267 K$ spruce pixels which are misclassified as broadleaf. Meaning, the number of true positives counts much less than the number of false positives  .   

Besides the precision also the accuracy is influenced by the uneven species representation. For instance, the accuracy of the tree specie Douglas-fir is $0.95$ although the precision and recall are zero. The reason for the poor precision and recall value is that the number of true positives is zero like shown in the confusion matrix depicted in Figure \ref{fig:cm_validation}. However, the $3 K$ Douglas-fir pixels are a small part of the total population of $464 K$ pixels. That's why the number of $441 K$ true negatives is sufficient enough to get an accuracy of $0.95$. Hence, one has to consider several different metrices to really assess the performance of a classification system.

After describing the results of our approach we compare them to the results of \cite{selina_thesis}. Figure \ref{fig:cm_comparison} shows the validation confusion matrices of both approaches. Except for the tree specie fir the results of \cite{selina_thesis} outperform our results. However, it should be taken into account that in our approach the resolution of the classification is higher than in \cite{selina_thesis}, since we predict the tree species pixel and not patch wise.

\begin{figure}[t]
  \begin{center}
  \subfigure[Ground truth pixels.]{
 \includegraphics[trim=0 0 20cm 0, scale=0.17, clip]{media/ground_truth_training.png}
  \label{fig:ground_truth_training} }   
  \subfigure[Prediction results for the pixels of (a).] {  
  \includegraphics[trim=0 0 20cm 0, scale=0.17, clip]{media/prediction_training.png}
  \label{fig:prediction_training}}   
  \caption{Prediction results on the training set.}
  \label{fig:exp_training} 
    \end{center}
\end{figure} 

\begin{figure}[t]
  \begin{center}
  \subfigure[Ground truth pixels.] {
   \includegraphics[trim=0 8cm 20cm 8cm, scale=0.2, clip]{media/ground_truth_testing.png}
  \label{fig:ground_truth_testing} } 
  \subfigure[Prediction results for the pixels of (a).]{
  \includegraphics[trim=0 8cm 20cm 8cm, scale=0.2, clip]{media/prediction_testing.png}
  \label{fig:prediction_testing}}  
   \subfigure[Prediction results for all pixels.]{
  \includegraphics[trim=0 8cm 20cm 8cm, scale=0.2, clip]{media/full_prediction_testing.png}
  \label{fig:prediction_full_testing}}
  \caption{Prediction results on the test set.}
  \label{fig:exp_testing}
  \end{center}
\end{figure} 

In addition to the quantitative results the Figure \ref{fig:exp_training} and Figure \ref{fig:exp_testing} show some prediction results on the training and test set respectively.
Most of the ground truth pixels depicted in Figure \ref{fig:ground_truth_training} are predicted correctly like shown in Figure \ref{fig:prediction_training}. Only the tree specie spruce is sometimes misclassified as broadleaf, fir or pine.

The prediction on the test set illustrated in Figure \ref{fig:prediction_testing} hardly differs from the ground truth shown in Figure \ref{fig:ground_truth_testing}. Solely, some  
pixels of the tree specie larch are assigned falsely to the tree specie broadleaf.

Our approach doesn't only provide a prediction for the ground truth pixels but for all pixels like shown in Figure \ref{fig:prediction_full_testing}. Figure \ref{fig:prediction_full_testing} also shows, that our approach can provide a prediction for the area of a tree specie.



\section{Conclusion}
We presented an approach which uses a fully convolutional neural network to provide a pixel wise classification of tree species. 

The network was trained with only few samples. To increase the robustness of our approach we added rotation as data augmentation and trained the network with orthophotos from two different months, namely April and July. 
Once the network is trained it can output for a given orthophoto and \gls{ndsm} a full segmentation map where each color represents a tree specie. This segmentation map also provides a prediction for the areas of the tree species.

We evaluated the performance on a training, test and validation set. The results on the training and test set were quite promising.
On the validation set the computed metrics precision, recall and accuracy were strongly influenced by the uneven tree species population. It may be reasonable to use a more balanced validation set in the future.

In the comparison on the validation set, the results of \cite{selina_thesis} outperform our results. 

A main problem could be that we use the 2x2m patches of \cite{selina_thesis} which are often not accurate enough to learn a per pixel segmentation of tree species. For instance, it can happen that a 2x2m patch of one tree specie also contains some pixels of another tree species which can lead to inconsistent training and test data.

Furthermore, the number of labelled pixels is relatively small compared to the overall number of pixels. Meaning,  a higher number of labels may help to improve the pixel wise classification. 

Apart from the results our approach has also some advantages over the one of \cite{selina_thesis}. For instance, no hand-crafted features are needed. An orthophoto and a \gls{ndsm} are enough.
Furthermore, the resolution of the classification of our approach is not limited to a fixed patch size. This also means that our approach needs no extra class like \cite{selina_thesis} to handle patches containing pixels of different tree species.
Moreover, our approach can use the information about nearby trees during the classification, since it considers pixels of neighboured trees at the same time. This is not possible in the approach of \cite{selina_thesis} since each patch is classified independently.


 
\glstoctrue
\glossarystyle{altlist}
\printglossaries

\newpage
\onecolumn
\bibliographystyle{plain}
\bibliography{ref}{}

\end{document}
